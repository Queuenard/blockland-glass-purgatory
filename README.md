These are add-ons which would be uploaded (or updated) on Blockland Glass if it weren't read-only for the time being


### Bot_PatrickStarFIXED.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Bot_PatrickStarFIXED.zip

	Fixed crashing; same reasons as Player_FarmAnimalsFIXED.zip

### Client_SnapshotServerAssets.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Client_SnapshotServerAssets.zip

	Allows you to snapshot a server's assets while connected to a client. Output includes file lengths of assets for weak association to files in cache.db.

### Player_FarmAnimalsFIXED.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Player_FarmAnimalsFIXED.zip

	Player_FarmAnimals by Phydeoux but with crash-causing missing sequences fixed

### Player_SpongebobFIXED.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Player_SpongebobFIXED.zip

	Player_Spongebob by lemurboy but with crash-causing typo in sequences fixed

### Server_DedicatedAudioFix.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_DedicatedAudioFix.zip

	Fixes $GuiAudioType, $SimAudioType and $MessageAudioType being unset on dedicated servers by using preload

### Server_EnvironmentSaverLoader.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_EnvironmentSaverLoader.zip

	Allows saving and loading of environments to/from files by in-game commands.
	
	Based on Server_EnvironmentAutoLoader.zip by Swollow which only supported one environment in one file.

### Server_GamemodeExtraAddons.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_GamemodeExtraAddons.zip

	Loads non-gamemode add-ons according to regular add-on enabling prefs
	
	Does not double-execute add-ons already executed by the gamemode
	
	Does not load music or prints

### Server_SpeedKartReduceCountdown.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SpeedKartReduceCountdown.zip

	Automatically set the pre-race timer on all maps to 15 seconds at round reset

### Server_SpeedKartRemasteredCommands.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SpeedKartRemasteredCommands.zip

	A single command for Speedkart Remastered where admins can change the race countdown with /setcountdown

### Server_SpeedKartRemasteredMapLoadFix.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SpeedKartRemasteredMapLoadFix.zip

	Safely deletes BrickGroup_888888 before each map load in order to prevent huge piles of dead named brick references from crowding the variable table

### Server_SpeedKartRemasteredMusicFix.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SpeedKartRemasteredMusicFix.zip

	Allows setting music before the race starts in Speedkart Remastered

### Server_SpeedKartRemasteredTracker.zip

https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SpeedKartRemasteredTracker.zip

	Add-on to record Speedkart Remastered players and vehicles from race start to end
	
	Might result in larger CPU usage and large disk space usage

### Server_SportsBallExploitFix.zip
	
https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/Server_SportsBallExploitFix.zip

	Adds a missing permission check to serverCmdSetMiniGameData because Item_Sports missed it
	
	Prevents an exploit where any player in a minigame can give all players balls and set the minigame's default ball

### System_Ultimate_Privacy.zip
	
https://gitlab.com/Queuenard/blockland-glass-purgatory/-/raw/main/System_Ultimate_Privacy.zip

	Succesor to Support\_Updater\_Privacy that greatly expands the tracking features blocked.


